import React from 'react'
import Layout from '../components/layout'
import Video from '../components/video'
import Header from '../components/header'

import './Home.css'

export default props => (
  <Layout>
    <Video />
    <Header />
  </Layout>
)